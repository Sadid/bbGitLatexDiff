# Bare Bone Git LaTeX Diff

This simple script get the input and create two snapshots of the git directory in given hash commits and create diff.tex in project folder to compile the pdf-diff file.

# Requirements

* Linux/Unix 
* Git
* [latexdiff](https://www.ctan.org/pkg/latexdiff)
* [latexpand](https://gitlab.com/latexpand/latexpand)

# Usage

```bash
	bbGitLatexDiff oldCommitHash newCommitHash mainFile.tex
```

# Also Consider
```bash
	git diff --word-diff HEAD~
```

# Alternatives

* **[git-latexdiff](https://gitlab.com/git-latexdiff/git-latexdiff)** The default one everybody should refer to.
* **[latexdiff-vc](https://github.com/cawka/latexdiff/blob/master/latexdiff-git)** The official version control based latexdiff shipped by latexdiff
* **[latexdiff-git](https://github.com/cawka/latexdiff/blob/master/latexdiff-git)**

# LICENSE
`GPLv3`
